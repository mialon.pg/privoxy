FROM alpine:3.21.3

# renovate: datasource=repology depName=alpine_3_21/privoxy versioning=loose
ENV PRIVOXY_VERSION="3.0.34-r3"

# renovate: datasource=repology depName=alpine_3_21/dumb-init versioning=loose
ENV DUMB_INIT_VERSION="1.2.5-r3"

RUN set -feux \
  && apk --no-cache add privoxy=${PRIVOXY_VERSION} \
                        dumb-init=${DUMB_INIT_VERSION}

USER privoxy

RUN set -feux \
  && for file in $(find /etc/privoxy -type f -name '*.new'); \
     do \
       mv ${file} $(echo $file | sed 's/\.new//g' ) ;\
     done

COPY config /

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/sbin/privoxy", "--no-daemon", "/config" ]
